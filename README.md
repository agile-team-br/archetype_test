# API Gestão de Processos Judiciais

Esse repositório apresenta um start através do docker-compose para levantar toda a stack. Além desse repositório existem mais dois repositórios que mostram o código do front e backend:

* Frontend >> https://gitlab.com/ricanalista/archetype_test_front
* Backend >> https://gitlab.com/ricanalista/archetype_test_api

Usei uma arquitetura diluída baseada em micro serviços, possuem alguns testes de integração na api do springboot.

## Instalação

_Prerequisitos:_
* docker version 1.10.3
* docker-compose version 1.6.2

1. Uma vez clonado esse repositório execute o comando no raiz do diretorio: (Obs.: certifique-se que as portas 8080 e 8082 de sua máquina local não estejam sendo usadas).

   `docker-compose up`
   
2. Uma vez o serviço levantado você poderá acessar os serviços nas sequintes urls:

   * Swagger >> http://localhost:8080/api-processos/swagger-ui.html
   * Frontend >> http://localhost:8082/#/
   * Kibana >> http://localhost:5601

3. Caso queira parar o serviço execute o comando:

   `docker-compose down`
   

## Stack of frameworks/technologies:

* Spring Boot
* Swagger
* Logback
* Logstash
* ElasticSearch
* Kibana
* Docker
* VueJs
* Quasar
* Liquibase
* Maven
* H2


## Débitos Técnicos:

* Funcionalidade email, cadastro processos, upload foto
* Kibana apresenta um bug pela versão, dependendo do ambiente está apresentando uma tela de login, mas está se comunicando com o spring.
* Poderia levantar uma outra stack motrando um build integrado com gitlab pipeline e sonar

